# MYOC Open Street Map!



Author: Andrei Stefan Bejgu (1691229)



# Project



The task was to generate a 3d city by using Yocto and I decided to employ Open Street Map to generate a pseudo-realistic representation of a city.

In particular, I used [OSMNX library](https://github.com/gboeing/osmnx), which allowed me to retrieve OSM information in Python.

I created a REST service using [Flask](https://flask.palletsprojects.com/en/1.1.x/) to communicate with the c++ part of the library integrated with Yocto. For this reason, I created an endpoint that, once given an address, returns a JSON containing the surrounding area along with 2D information about buildings and footprints.



## Installation OSMNX (python)



The python part of the project is composed of a file:

```

city_generator_python/rest_city.py

```

To run this tool you can create a conda environment by using the following commands and then installing all the requirements.

```

conda config --prepend channels conda-forge

conda create -n ox --strict-channel-priority osmnx

conda activate ox

pip install Flask

```

After these commands are performed a conda environment, called "ox", will be generated.

The environment will be activated by running the following command.

```

conda activate ox

```

The following python command can be run to start the flask server and to perform rest queries.

```

python3 rest_city.py

```



### City generator endpoint



The endpoint accepts an *address* parameter which indicates the address where it is possible to retrieve the OSM information.

Example:

```

Piazzale aldo moro 6 Roma

```

```

Colosseo

```

ecc.

The endpoint is going to answer with a JSON containing the 2d information obtained by  OSM.



#### OSM retrieved informations



OSM allows to retrieve Geospatial information. For each of the following structures, I received the geospatial GPS coordinates of the points or geometries involved and then converted and scaled all of them to local values in order to generate a 3D scene.

The information retrieved through OSM are:

* ####  Buildings

Form OSM I retrieved information about buildings thanks to which I could retrieve the geometries (polygons) and height of the edifices.

Since, unfortunately, not all buildings had information about the number of floors, I decided to use the height in meters (when provided) and convert it to the number of floors, in order to generate a uniform representation of the city. This was done by dividing the height by 3.2 (the average height of a floor).

In case the number of levels or height wasn't provided, I tried to generate the height of the building automatically. For example, we know that the *apartment* building type usually has more than 1 floor. For this reason, I used the Numpy function to generate a random number between 8 and 10, which would represent the levels of the building.

In case there was no information about the level number, height, or building type, I automatically assumed the building had only 1 level.

I also tried to use the average levels of the buildings in the scene and assign them to unknown buildings, but it didn't produce aesthetically pleasing cities because residential houses turned out too high.

* #### Land use of the area

From OSM I retrieved information about the use of the areas surrounding the address. This was useful in order to provide the areas different colors, depending on the scene. For example, if the area is a forest or a park, the green color will make the city look more real.

*  #### Water information (river or lakes)

From OSM I also retrieved information about rivers, lakes, canals, or other water sources that cross the area.

* #### Roads

OSM provides the Multigraph of the roads of the specified area. Through this Graph, I generated an approximation of the roads in a 2D space because, unfortunately, OSM doesn't provide the polygon geometry of the road but only the extremes that define it.

I created an algorithm that, when given two points in a GPS space (2D), generates a thick polygon. I distinguished some kinds of roads: for example, normal ones are different from *paths*, which are going to be tighter.



In the end, I am going to obtain a collection of geometrical objects with both different shapes and heights. Since these objects lay in the real world's GPS space, I converted them to local coordinates in order to generate the local scene while still maintaining the proportions.

After that, I assigned to each object a material containing the color of the item.

These objects are going to be converted to JSON and provided to the caller through the REST APIS.

An example of JSON is:

```

{

   "buildings":{

      "building_271711150":{

         "level":0.275,

         "type":"building",

         "coords":[

            -7.429697602887792,

            10.451095057697799,

            -7.296453646811184,

            10.061619923086425

         ],

         "name":"building_271711150"

      },

      "building_487730440":{

         "level":0.175,

         "type":"building",

         "coords":[

            -17.469524668854046,

            -16.45883624508187,

            -17.434318674976332,

            -16.4683624508187

         ],

         "name":"building_487730440"

      }

   },

   "materials":{

      "building":{

         "color":[

            0.79,

            0.74,

            0.62

         ],

         "type":"building",

         "sub_type":""

      },

      "forest":{

         "color":[

            0.337,

            0.49,

            0.274

         ],

         "type":"forest",

         "sub_type":""

      }

   }

}



```

This JSON is then used to generate a 3D scene by using YOCTO.



## City generator (c++)



### Installation



The app can be compiled by using the Yocto standard compilation procedure and will generate a file "bin/icity_generator" that could be used to start the user interface and search for any address.



### Implementation

The scene generator is integrated into Yocto as an app. I took *ysceneitrace* and modified it, creating a visual application that allows to search for an address and then renders the area surrounding it in a 3D scene.

I created a cpp file called *icity_generator.cpp*  in the folder *apps/city_generator*. This class uses Yocto to render the scene and communicates with the python REST service through HTTP requests.

The HTTP communication is made by using the library [HTTPRequest](https://github.com/elnormous/HTTPRequest): the file is found in the ext folder.

When given an address through the user interface, the software is going to communicate with the Python server and then render the 3D scene of the obtained data.

The given JSON, produced by the Python server, is processed by using the [JSON library](https://github.com/nlohmann/json): the file is found in the ext folder and allows to parse the JSON.

For each polygon returned in the JSON, an earcut algorithm is used to fill the polygon with triangles to generate a 3D model of it, which can be represented using Yocto.

I tried to implement the earcut algorithm myself but I obtained some bugs due to some odd building's shapes, so I preferred to use an existing library the professor suggested ([Earcut library](https://github.com/mapbox/earcut.hpp)).

For what concerns the buildings, the roof, composed by generated triangles, is going to be placed in the given coordinates and at the given height. Then, for each side of the roof, a wall is going to be generated by creating 2 triangles to fill it.

I also tried to add textures to the buildings but the results were not that satisfying since, in OSM, not all buildings are classified, which makes it more difficult to assign specific textures to different building categories.

When the 3D shape of each object is generated, some special characteristics are assigned to each object. So, for example, the water is given the transparency and reflection to appear more real.



### Usage



Running the following command

```

./bin/icity_generator

```

a user interface will be created with a default scene loaded. Here is an example of the interface.



#### UI



![UI example](images/city_generator/ui_example.png "Ui example")

In the **address** box on the left, you can search the address you prefer and then click on the **search** button to visualize the rendered 3D model of the selected area.

You also have all the *yoctosceneitrace*'s functionalities, for example you can save the generated model.



### Examples



Here are some examples of the generated city. All the models are also stored in the folder:

```

city_generator_examples

```



#### Rome



Here is an example of the scene generated by searching Rome's center.

![UI example](images/city_generator/colosseo_example_2.jpg "Rome")

Unfortunately, as you can see, not all the buildings have the height, since OSM doesn't have data for all of the edifices, so the city result is not fully uniform.



#### Paris



Here is an example of the scene generated by searching for Paris' center.

![UI example](images/city_generator/paris_example.jpg "Paris")

It is one of the cities with the most detailed information about building's heights, so the result is aesthetically pleasing.



#### Eur Fermi (Roma)



In this picture, we can see the model generated by searching for Eur Fermi (Rome), chosen for its high buildings alongside lower houses, and the nice lake located in the center of the area.

![UI example](images/city_generator/eur_fermi_example.jpg "Eur Fermi")

This example encloses all the features of the generator.



#### Manhattan



In this picture, we can see the model generated by searching for Manhattan, which has several skyscrapers. Here I set a **random height** to the buildings since only a few of them had the required information.

![UI example](images/city_generator/manhattan_example_random_height_different_sky.jpg "Manhattan")

Unfortunately, I couldn't control the generation of the building's height inside Central Park, so the results are a bit unpleasant.



#### Amsterdam



In this picture, we can see the model generated by searching for Amsterdam, chosen for its particular shape due to the multitude of canals.

![UI example](images/city_generator/amsterdam_example.jpg "Amsterdam")



#### Monaco



In this picture, we can see the model generated by searching for Monaco, which is one of the most complete OSM's cities.

![UI example](images/city_generator/monaco_example.jpg "Monaco")

The model has the biggest number of 3D objects (**90000**).



## Issues



One of the biggest issues of the software is that the OSM queries are relatively slow. In fact, to retrieve, for example, the Rome center it takes almost **1 minute**, which is not that much considering the number of objects ( **65000**) and also the fact that I retrieved all data in a **2km range**. I would have liked to create a real-time render, able to generate the city while you are visiting it, but with the given APIs, it would have been nearly impossible.

Another issue is that there is no information about the sea level height, so it is impossible to represent hills and mountains in an aesthetically pleasing way.

Moreover, the API doesn't provide fair information about the sea so the cities near it turned out a bit odd.

Here is an example of **Ostia**, which is full of buildings by the sea, even if the latter is not represented.

![UI example](images/city_generator/ostia_example.jpg "Ostia")
